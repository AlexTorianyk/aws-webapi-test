# **AWS + .NET Test Instructions**

## Overview

This document explains the steps needed to accomplish  **AWS Cloud + .NET**.

The test is comprehended with **4 steps** that should be executed in a specific order:

1. Build a Serverless App / Database
2. Modify an existent .NET Web API accordingly
3. Deploy to EC2 following the instructions
4. Fill the gaps in the cloudformation script

Besides you will have to detail in [aws-work-log.md](aws-work-log.md) file, the steps you took to achieve the expected result.

**Note:** Feel free to write everything you consider important. Also screenshots are more than welcome.

---

## Prerequisites

1. Create an AWS Account for practice test:  https://aws.amazon.com/free
2. Install and configure the aws cli
3. Configure User profile creating IAM user
4. The AWS Toolkit for Visual Studio: https://aws.amazon.com/visualstudio/

---

## Part 1: Build a Serverless App using API Gateway, Lambda, and DynamoDB

In this use case, you will have to implement and deploy the following architecture:

![Serverless Infra solution](aws-resources/serverless-infra.png)

The application uses Amazon API Gateway to provide a RESTful API that can be called from an Web API. The API Gateway should redirects the requests to correspondent AWS Lambda function.

- The first lambda will query/add Reservations from Amazon DynamoDB.
- The second lambda will query Flights an Amazon DynamoDB.

> For the sake of complexity, your lambdas can return the full database content. 

The model data is here [data](aws-resources/lambda-data.json).

After you finished, please copy your source code into aws-resources/lambda-src.

+ Reservation.cs (c# code of first lambda)
+ Flight.cs (c# code of second lambda)
+ swagger.yaml (OpenAPI file using AWS API Gateway)
+ *Optional* serverless.template file (it's up to you to include this file)

> Fill out Part 01 in aws-work-log.md properly.

---

## Part 2: Modify and fill the gaps on a .NET Web API

The endpoints are already createdd as well the base infrastructure
The API exposes the following operations:

The source code is located in the [aws-webapi](aws-webapi) folder.

- [GET /Flight](aws-webapi/src/Ryanair.Reservation/Controllers/FlightController.cs): used to search for available flights on a certain date between two different locations.
- [POST /Reservation](aws-webapi/src/Ryanair.Reservation/Controllers/ReservationController.cs): used to create a reservation in the system
- [GET /Reservation](aws-webapi/src/Ryanair.Reservation/Controllers/ReservationController.cs): used to retrieve a reservation previously made.

We expected that you implement the following business rules correctly based on the system constraints we provide.

**System constraints:**

- There is a **maximum of 50 bags** per flight in total for all the passengers
- Each passenger can have a maximum of 5 bags per flight.
- There are **50** seats available per flight, numbered sequentially: "01", "02"… "50".
- Every endpoint should return appropriate error messages when the operation cannot be achieved for some reason. 

The Repositories classes are empty and should be modified to query from the lambdas you create earlier. 

*As it will be a HTTP request, so would be nice see some sort strategy in case lambdas stop responding.*

> You are free to do whatever you want on this solution except changing the endpoints and the input values. Feel free to show us what you can do best here.

---

## Part 3: Deploy Ryanair API on EC2 instances

We want to see what will be your approach deploying the .NET Web API into an EC2 instance achieving the following goals:

1. Capacity provisioning
1. Load balancing
1. Auto-scaling
1. Health monitoring
1. Logs Infrastructure

> Create an architecture diagram (could be exported as jpg or png) of your components/solution and their relation between them. Include it into aws-screenshot folder.

> Fill out Part 03 in aws-work-log.md properly.
---

## Part 4: Fill the gaps in the cloudformation script

In this part you have to create resources using yaml [file](aws-resources/03-cloudformation-script.yaml).

Read the comments and complete the resources to deploy one EC2 instance with Elastic IP.

---

## Submission

Once you finished your test, commit everything and share with us your fork leting your recruiter knows.

Thanks for your time, we look forward to hearing from you!

Ryanair Reservation Team

﻿namespace Ryanair.Reservation.Common
{
    public interface IInternalEventHandler
    {
        void Handle(object @event);
    }
}
﻿using System;
using System.Reflection;

namespace Ryanair.Reservation.Common
{
    public class InvalidValueException : Exception
    {
        public InvalidValueException(MemberInfo type, string message)
            : base($"Value of {type.Name} {message}")
        {
        }
    }
}
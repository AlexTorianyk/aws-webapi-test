﻿using System;
using Amazon.DynamoDBv2.DataModel;

namespace Lambdas
{
    [DynamoDBTable("Flight")]
    public class FlightDto
    {
        public string Key { get; set; }

        public string Origin { get; set; }

        public string Destination { get; set; }

        public DateTime Time { get; set; }

        public FlightDto(string key, string origin, string destination, DateTime time)
        {
            Key = key;
            Origin = origin;
            Destination = destination;
            Time = time;
        }
        
        public FlightDto()
        {
            
        }
    }
}
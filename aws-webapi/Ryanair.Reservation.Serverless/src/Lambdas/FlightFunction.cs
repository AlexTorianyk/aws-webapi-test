using System.Collections.Generic;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace Lambdas
{
    public class FlightFunction
    {
        private static readonly AmazonDynamoDBClient _dbClient = new AmazonDynamoDBClient();
        
        public static async Task<APIGatewayProxyResponse> GetFlightsHandler(ILambdaContext context)
        {
            using var dbContext = new DynamoDBContext(_dbClient);
            var flights = await dbContext.ScanAsync<FlightDto>(new List<ScanCondition>()).GetRemainingAsync();
            
            return new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonSerializer.Serialize(flights),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };
        }
    }
}

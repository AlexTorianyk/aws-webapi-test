﻿namespace Lambdas
{
    public class PassengerDto
    {
        public string Name { get; }

        public int Bags { get; }

        public string Seat { get; }
    }
}
﻿using System.Collections.Generic;
using Amazon.DynamoDBv2.DataModel;

namespace Lambdas
{
    [DynamoDBTable("Reservation")]
    public class ReservationDto
    {
        public string Id { get; private set; }

        public string ReservationNumber { get; internal set; }

        public string Email { get; internal set; }

        public string CreditCard { get; internal set; }

        public FlightDto InboundFlight { get; internal set; }

        public FlightDto OutboundFlight { get; internal set; }
        
        public List<PassengerDto> PassengersInboundFlight { get; private set; }

        public List<PassengerDto> PassengersOutboundFlight { get; private set; }

        public bool IsRoundTrip => this.InboundFlight != null;

        public ReservationDto(string id, string reservationNumber, string email, string creditCard, FlightDto inboundFlight, FlightDto outboundFlight, List<PassengerDto> passengersInboundFlight, List<PassengerDto> passengersOutboundFlight)
        {
            Id = id;
            ReservationNumber = reservationNumber;
            Email = email;
            CreditCard = creditCard;
            InboundFlight = inboundFlight;
            OutboundFlight = outboundFlight;
            PassengersInboundFlight = passengersInboundFlight;
            PassengersOutboundFlight = passengersOutboundFlight;
        }

        public ReservationDto()
        {
            
            
        }
    }
}
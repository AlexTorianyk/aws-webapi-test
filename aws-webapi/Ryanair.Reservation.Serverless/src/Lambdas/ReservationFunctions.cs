﻿using System.Collections.Generic;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;

namespace Lambdas
{
    public class ReservationFunctions
    {
        private static readonly AmazonDynamoDBClient _dbClient = new AmazonDynamoDBClient();
        
        public static async Task<APIGatewayProxyResponse> GetReservationsHandler(ILambdaContext context)
        {
            using var dbContext = new DynamoDBContext(_dbClient);
            var reservations = await dbContext.ScanAsync<ReservationDto>(new List<ScanCondition>()).GetRemainingAsync();
            
            return new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonSerializer.Serialize(reservations),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };
        }
        
        public static async Task<APIGatewayProxyResponse> AddReservationHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            var reservation =
                JsonSerializer.Deserialize<ReservationDto>(request.Body);
            
            using var dbContext = new DynamoDBContext(_dbClient);
            await dbContext.SaveAsync(reservation);

            return new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };
        }
    }
}
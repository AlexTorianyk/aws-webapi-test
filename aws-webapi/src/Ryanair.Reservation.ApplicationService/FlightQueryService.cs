﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ryanair.Reservation.ApplicationService.Interfaces;
using Ryanair.Reservation.Domain.Flights;

namespace Ryanair.Reservation.ApplicationService
{
    public class FlightQueryService : IFlightQueryService
    {
        private readonly IFlightRepository _flightRepository;

        public FlightQueryService(IFlightRepository flightRepository)
        {
            _flightRepository = flightRepository;
        }

        public async Task<IEnumerable<Flight>> Get(int passengers, string origin, string destination, DateTime dateOut, DateTime dateIn, bool roundTrip)
        {
            return await _flightRepository.GetOneWay(origin, destination, dateOut);
        }
    }
}

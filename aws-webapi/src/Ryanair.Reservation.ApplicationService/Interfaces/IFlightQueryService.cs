﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ryanair.Reservation.Domain.Flights;

namespace Ryanair.Reservation.ApplicationService.Interfaces
{
    public interface IFlightQueryService
    {
        Task<IEnumerable<Flight>> Get(int passengers, string origin, string destination, DateTime dateOut,
            DateTime dateIn, bool roundTrip);
    }
}

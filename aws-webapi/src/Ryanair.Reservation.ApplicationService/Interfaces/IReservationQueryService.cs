﻿namespace Ryanair.Reservation.ApplicationService.Interfaces
{
    public interface IReservationQueryService
    {
        Domain.Reservations.Reservation GetByReservationKey(string reservationKey);
    }
}

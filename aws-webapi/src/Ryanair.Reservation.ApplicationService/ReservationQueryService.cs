﻿using Ryanair.Reservation.ApplicationService.Interfaces;
using Ryanair.Reservation.Domain.Reservations;

namespace Ryanair.Reservation.ApplicationService
{
    public class ReservationQueryService : IReservationQueryService
    {
        private readonly IReservationRepository _reservationRepository;

        public ReservationQueryService(IReservationRepository reservationRepository)
        {
            _reservationRepository = reservationRepository;
        }

        public Domain.Reservations.Reservation GetByReservationKey(string reservationKey)
        {
            return _reservationRepository.GetByReservationKey(reservationKey);
        }
    }
}

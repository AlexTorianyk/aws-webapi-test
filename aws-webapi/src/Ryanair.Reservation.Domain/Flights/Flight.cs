﻿using System.Collections.Generic;
using System.Linq;
using Amazon.DynamoDBv2.DataModel;
using Ryanair.Reservation.Common;
using Ryanair.Reservation.Domain.Flights.Key;
using Ryanair.Reservation.Domain.Flights.Terminal;
using Ryanair.Reservation.Domain.Flights.Time;
using Ryanair.Reservation.Domain.Shared.Exceptions;
using Ryanair.Reservation.Domain.Shared.Passenger;

namespace Ryanair.Reservation.Domain.Flights
{
    public class Flight : AggregateRoot
    {
        [DynamoDBProperty(typeof(FlightKeyConverter))]
        public FlightKey Key { get; set; }
        [DynamoDBProperty(typeof(FlightTerminalConverter))]
        public FlightTerminal Origin { get; set; }
        [DynamoDBProperty(typeof(FlightTerminalConverter))]
        public FlightTerminal Destination { get; set; }
        [DynamoDBProperty(typeof(FlightTimeConverter))]
        public FlightTime Time { get; set; }
        public IList<Passenger> Passengers { get; set; }
        public int Bags => Passengers?.Sum(passenger => passenger.Bags) ?? 0; 

        public Flight(FlightKey key, FlightTerminal origin, FlightTerminal destination, FlightTime time)
        {
             Passengers = new List<Passenger>();
            Apply(new FlightEvents.Created
            {
                Key = key,
                Origin = origin,
                Destination = destination,
                Time = time
            });
        }

        public void Reschedule(FlightTime time)
        {
            Apply(new FlightEvents.Rescheduled
            {
                Time = time
            });
        }

        public void AddPassenger(Passenger passenger)
        {
            Apply(new FlightEvents.PassengerAdded()
            {
                Passenger = passenger
            });
        }
        
        protected override void When(object @event)
        {
            switch (@event)
            {
                case FlightEvents.Created e:
                    Key = e.Key;
                    Origin = e.Origin;
                    Destination = e.Destination;
                    Time = e.Time;
                    break;
                case FlightEvents.Rescheduled e:
                    Time = e.Time;
                    break;
                case FlightEvents.PassengerAdded e:
                    // Passengers.Add(e.Passenger);
                    break;
            }
        }

        protected override void EnsureValidState()
        {
            var valid = Origin != Destination && Time != FlightTime.NoTime && LessThanFiftyBags();
            if (!valid)
                throw new Exceptions.InvalidEntityState(this,
                    $"Post-checks failed for Flight {Key}");
        }

        private bool LessThanFiftyBags()
        {
            return (!Passengers.Any() || Bags <= 50);
        }

        public Flight()
        {
            
        }
    }
}

﻿using Ryanair.Reservation.Domain.Flights.Key;
using Ryanair.Reservation.Domain.Flights.Terminal;
using Ryanair.Reservation.Domain.Flights.Time;
using Ryanair.Reservation.Domain.Shared.Passenger;

namespace Ryanair.Reservation.Domain.Flights
{
    public static class FlightEvents
    {
        public class Created
        {
            public FlightKey Key { get; set; }
            public FlightTerminal Origin { get; set; }
            public FlightTerminal Destination { get; set; }
            public FlightTime Time { get; set; }
        }

        public class Rescheduled
        {
            public FlightTime Time { get; set; }
        }

        public class PassengerAdded
        {
            public Passenger Passenger { get; set; }
        }
    }
}
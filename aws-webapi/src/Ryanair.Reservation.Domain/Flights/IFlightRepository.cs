﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ryanair.Reservation.Domain.Flights
{
    public interface IFlightRepository
    {
        Task<IEnumerable<Flight>> GetOneWay(string origin, string destination, DateTime dateOut);

        Task<IDictionary<Flight, IEnumerable<Flight>>> GetTwoWay(string origin, string destination, DateTime dateOut,
            DateTime dateIn);

    }
}

﻿namespace Ryanair.Reservation.Domain.Flights
{
    // implementation of this would check whether the terminal name is valid and not some gibberish
    public interface ITerminalValidator
    {
        bool IsValidTerminal(string terminal);
    }
}
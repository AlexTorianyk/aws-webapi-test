﻿using System;
using Ryanair.Reservation.Common;

namespace Ryanair.Reservation.Domain.Flights.Key
{
    public class FlightKey : Value<FlightKey>
    {
        public string Value { get; internal set; }
        
        protected FlightKey() { }

        public FlightKey(string value)
        {
            CheckValidity(value);

            Value = value;
        }

        private static void CheckValidity(string value)
        {
            if (value.IsEmpty())
                throw new ArgumentNullException(nameof(value), "Flight Key cannot be empty");
        }

        public static FlightKey FromString(string key)
        {
            if (key.IsEmpty())
                throw new ArgumentNullException(nameof(key));

            return new FlightKey(key);
        }

        public static implicit operator string(FlightKey self) => self.Value;
    }
}
﻿using System;
using Ryanair.Reservation.Common;

namespace Ryanair.Reservation.Domain.Flights.Terminal
{
    public class FlightTerminal : Value<FlightTerminal>
    {
        public string Value { get; internal set; }

        protected FlightTerminal() { }

        public static FlightTerminal FromString(string value, ITerminalValidator terminalValidator)
        {
            CheckValidity(value, terminalValidator);
            return new FlightTerminal(value);
        }
        
        public static FlightTerminal FromString(string value)
        {
            CheckValidity(value);
            return new FlightTerminal(value);
        }

        // setting the value to the upper case so it matches the file you provided
        internal FlightTerminal(string value) => Value = value.ToUpper();

        public static implicit operator string(FlightTerminal terminal) => terminal.Value;

        private static void CheckValidity(string value, ITerminalValidator terminalValidator)
        {
            if (value.IsEmpty())
                throw new ArgumentNullException(nameof(FlightTerminal));

            if (value.Length < 2)
                throw new ArgumentOutOfRangeException(nameof(value), "Terminal is too short");

            if (!terminalValidator.IsValidTerminal(value))
                throw new InvalidValueException(typeof(FlightTerminal), "Terminal is not valid");
        }
        
        private static void CheckValidity(string value)
        {
            if (value.IsEmpty())
                throw new ArgumentNullException(nameof(FlightTerminal));

            if (value.Length < 2)
                throw new ArgumentOutOfRangeException(nameof(value), "Terminal is too short");
        }

        public static FlightTerminal NoTerminal => new FlightTerminal();
    }
}
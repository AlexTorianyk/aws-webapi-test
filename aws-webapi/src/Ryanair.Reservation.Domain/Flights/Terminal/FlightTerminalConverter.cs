﻿using System;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;

namespace Ryanair.Reservation.Domain.Flights.Terminal
{
    public class FlightTerminalConverter : IPropertyConverter
    {
        public DynamoDBEntry ToEntry(object value)
        {
            var terminal = value as FlightTerminal;

            return new Primitive
            {
                Value = terminal?.Value
            };
        }

        public object FromEntry(DynamoDBEntry entry)
        {
            var primitive = entry as Primitive;
            if (!(primitive?.Value is string) || string.IsNullOrEmpty((string)primitive.Value))
                throw new ArgumentOutOfRangeException();

            return FlightTerminal.FromString(primitive);
        }
    }
}
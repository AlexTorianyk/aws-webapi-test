﻿using System;
using Ryanair.Reservation.Common;

namespace Ryanair.Reservation.Domain.Flights.Time
{
    public class FlightTime : Value<FlightTime>
    {
        private const int OneDay = 1;
        public DateTimeOffset Value { get; internal set; }

        protected FlightTime() {}
        internal FlightTime(DateTimeOffset value) => Value = value;

        public static FlightTime FromDateTimeOffset(DateTimeOffset submissionDate)
        {
            CheckValidity(submissionDate);
            return new FlightTime(submissionDate);
        }

        private static void CheckValidity(DateTimeOffset value)
        {
            if (value == default)
                throw new ArgumentNullException(nameof(value), "Flight time cannot be empty");
        }

        public static implicit operator DateTimeOffset(FlightTime submissionDate)
            => submissionDate.Value;

        public static FlightTime NoTime => new FlightTime();

    }
}
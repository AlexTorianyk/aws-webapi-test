﻿namespace Ryanair.Reservation.Domain.Reservations
{
    public interface IReservationRepository 
    {
        Reservation GetByReservationKey(string reservationKey);
    }
}

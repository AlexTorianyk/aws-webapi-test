﻿using System.Collections.Generic;
using Ryanair.Reservation.Domain.Flights;
using Ryanair.Reservation.Domain.Shared.Passenger;

namespace Ryanair.Reservation.Domain.Reservations
{
    public class Reservation
    {
        public int Id { get; private set; }

        public string ReservationNumber { get; internal set; }

        public string Email { get; internal set; }

        public string CreditCard { get; internal set; }

        public Flight InboundFlight { get; internal set; }

        public Flight OutboundFlight { get; internal set; }
        
        public List<Passenger> PassengersInboundFlight { get; private set; }

        public List<Passenger> PassengersOutboundFlight { get; private set; }

        public bool IsRoundTrip => InboundFlight != null;
    }
}

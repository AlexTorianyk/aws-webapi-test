﻿using System;
using Ryanair.Reservation.Common;

namespace Ryanair.Reservation.Domain.Shared.Passenger.Bags
{
    public class PassengerBags : Value<PassengerBags>
    {
        public int Value { get; internal set; }

        protected PassengerBags() { }

        internal PassengerBags(int value) => Value = value;

        public static PassengerBags FromString(int value)
        {
            if (value < 0)
                throw new ArgumentOutOfRangeException(nameof(value), "Passenger can't have negative amount of bags");
            
            if (value > 5)
                throw new ArgumentOutOfRangeException(nameof(value), "Passenger can't have more than five bags");

            return new PassengerBags(value);
        }

        public static implicit operator int(PassengerBags value)
            => value.Value;

        public static PassengerBags NoPassengerBags => new PassengerBags();
    }
}
﻿using System;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;

namespace Ryanair.Reservation.Domain.Shared.Passenger.Bags
{
    public class PassengerBagsConverter : IPropertyConverter
    {
        public DynamoDBEntry ToEntry(object value)
        {
            var terminal = value as PassengerBags;

            return new Primitive
            {
                Value = terminal?.Value
            };
        }

        public object FromEntry(DynamoDBEntry entry)
        {
            var primitive = entry as Primitive;
            if (!(primitive?.Value is int))
                throw new ArgumentOutOfRangeException();

            return PassengerBags.FromString(int.Parse(primitive));
        }
    }
}
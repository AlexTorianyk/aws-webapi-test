﻿using System;
using Ryanair.Reservation.Common;

namespace Ryanair.Reservation.Domain.Shared.Passenger.Name
{
    public class PassengerName : Value<PassengerName>
    {
        public string Value { get; internal set; }

        protected PassengerName() { }

        internal PassengerName(string value) => Value = value;

        public static PassengerName FromString(string value)
        {
            if (value.IsEmpty())
                throw new ArgumentNullException(nameof(value));

            return new PassengerName(value);
        }

        public static implicit operator string(PassengerName value)
            => value.Value;

        public static PassengerName NoPassengerName => new PassengerName();
    }
}
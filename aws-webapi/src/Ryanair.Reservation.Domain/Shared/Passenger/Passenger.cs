﻿using Amazon.DynamoDBv2.DataModel;
using Ryanair.Reservation.Common;
using Ryanair.Reservation.Domain.Shared.Passenger.Bags;
using Ryanair.Reservation.Domain.Shared.Passenger.Name;
using Ryanair.Reservation.Domain.Shared.Passenger.Seat;

namespace Ryanair.Reservation.Domain.Shared.Passenger
{
    public class Passenger : Entity
    {
        [DynamoDBProperty(typeof(PassengerNameConverter))]
        public PassengerName Name { get; set; }

        [DynamoDBProperty(typeof(PassengerBagsConverter))]
        public PassengerBags Bags { get; set; }

        [DynamoDBProperty(typeof(SeatConverter))]
        public Seat.Seat Seat { get; set; }
        
        protected override void When(object @event)
        {
            throw new System.NotImplementedException();
        }
    }
}

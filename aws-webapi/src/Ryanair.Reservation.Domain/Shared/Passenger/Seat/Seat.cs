﻿using System;
using Ryanair.Reservation.Common;

namespace Ryanair.Reservation.Domain.Shared.Passenger.Seat
{
    public class Seat : Value<Seat>
    {
        public string Value { get; internal set; }
        public bool Taken { get; private set; }

        protected Seat() { }

        internal Seat(string value)
        {
            Value = value;
            Taken = true;
        } 

        public static Seat FromString(string value)
        {
            if (value.IsEmpty())
                throw new ArgumentNullException(nameof(value));

            return new Seat(value);
        }

        public static implicit operator string(Seat value)
            => value.Value;

        public static Seat NoSeat => new Seat();
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Ryanair.Reservation.Domain.Flights;

namespace Ryanair.Reservation.Flights
{
    [Route("[controller]")]
    [ApiController]
    public class FlightController : ControllerBase
    {
        private readonly IFlightRepository _flightRepository;

        public FlightController(IFlightRepository flightRepository)
        {
            _flightRepository = flightRepository;
        }

        [HttpGet]
        [Route("one-way")]
        public async Task<IEnumerable<Flight>> GetOneWay([FromQuery] GetFlights getFlights)
        {
            return await _flightRepository.GetOneWay(getFlights.Origin, getFlights.Destination, getFlights.DateOut);
        }
        
        [HttpGet]
        [Route("two-way")]
        public async Task<IDictionary<Flight, IEnumerable<Flight>>> GetTwoWay([FromQuery] GetFlights getFlights)
        {
            return await _flightRepository.GetTwoWay(getFlights.Origin, getFlights.Destination, getFlights.DateOut, getFlights.DateIn);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Ryanair.Reservation.Domain.Flights;
using Ryanair.Reservation.Infrastructure.AutomaticDependencyInjection;

namespace Ryanair.Reservation.Flights
{
    public class FlightRepository : IFlightRepository, ISingleton
    {
        private static readonly AmazonDynamoDBClient _dbClient = new AmazonDynamoDBClient();
        
        public async Task<IEnumerable<Flight>> GetOneWay(string origin, string destination, DateTime dateOut)
        {
            var flights = await GetAll();

            flights = flights.Where(flight =>
                flight.Origin == origin && flight.Destination == destination && flight.Time.Value >= dateOut);
            
            return flights;
        }
        
        public async Task<IDictionary<Flight, IEnumerable<Flight>>> GetTwoWay(string origin, string destination, DateTime dateOut, DateTime dateIn)
        {
            var allFlights = await GetAll();
            
            var inboundFlights = await GetOneWay(origin, destination, dateOut);

            var flights = new Dictionary<Flight, IEnumerable<Flight>>();
            foreach (var inboundFlight in inboundFlights)
            {
                var outboundFlights = allFlights.Where(flight => flight.Origin == destination && flight.Destination == origin &&
                                                                 flight.Time.Value >= dateOut && flight.Time.Value <= dateIn);

                if (outboundFlights.Any())
                {
                    flights.Add(inboundFlight, outboundFlights);
                }
            }
            
            return flights;
        }
        
        private static async Task<IEnumerable<Flight>> GetAll()
        {
            using var dbContext = new DynamoDBContext(_dbClient);
            var flights = await dbContext.ScanAsync<Flight>(new List<ScanCondition>()).GetRemainingAsync();
            
            return flights;
        }
    }
}
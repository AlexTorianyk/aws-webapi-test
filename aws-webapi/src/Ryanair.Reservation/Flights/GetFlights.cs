﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Ryanair.Reservation.Flights
{
    public class GetFlights
    {
        public int Passengers { get; set; }

        [Required]
        public string Origin { get; set; }

        [Required]
        public string Destination { get; set; }

        [Required]
        public DateTime DateOut { get; set; }

        public DateTime DateIn { get; set; }

        public bool RoundTrip { get; set; }
    }
}

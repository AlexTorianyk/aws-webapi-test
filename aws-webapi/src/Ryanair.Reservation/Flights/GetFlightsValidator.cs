﻿using FluentValidation;
using Ryanair.Reservation.Infrastructure.AutomaticDependencyInjection;

namespace Ryanair.Reservation.Flights
{
    public class GetFlightsValidator : AbstractValidator<GetFlights>, ITransient
    {
        // you can add a lot more rules here
        public GetFlightsValidator()
        {
            RuleFor(flight => flight.Origin).NotEqual(flight => flight.Destination);
            RuleFor(flight => flight.DateOut).LessThan(flight => flight.DateIn);
            RuleFor(flight => flight.Passengers).LessThan(50);
        }
    }
}
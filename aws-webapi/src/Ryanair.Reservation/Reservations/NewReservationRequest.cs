﻿using System.Collections.Generic;

namespace Ryanair.Reservation.Reservations
{
    public class NewReservationRequest
    {
        public string Email { get; set; }

        public string CreditCard { get; set; }

        public List<ReservationFlight> Flights { get; set; }

        public bool IsRoundTrip => Flights?.Count == 2;

        public ReservationFlight GetOutBoundFlight() => Flights[0];

        public ReservationFlight GetInBoundFlight() => IsRoundTrip ? Flights[1] : null;
    }
}

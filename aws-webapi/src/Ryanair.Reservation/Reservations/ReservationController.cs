﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Ryanair.Reservation.ApplicationService.Interfaces;
using Ryanair.Reservation.Domain.Shared.Passenger;

namespace Ryanair.Reservation.Reservations
{
    [Route("[controller]")]
    [ApiController]
    public class ReservationController : ControllerBase
    {
        private readonly IReservationService _service;
        private readonly IReservationQueryService _queryService;

        public ReservationController(IReservationService service, IReservationQueryService queryService)
        {
            _service = service;
            _queryService = queryService;
        }

        [HttpGet("{key}")]
        public ActionResult<Domain.Reservations.Reservation> Get(string key)
        {
            var reservation = _queryService.GetByReservationKey(key);

            if (reservation == null)
            {
                return NotFound();
            }

            throw new NotImplementedException();
            // return new Domain.Reservations.Reservation
            // {
            //     ReservationNumber = reservation.ReservationNumber,
            //     Email = reservation.Email,
            //     Flights = new List<ReservationFlight>() //TODO: Add some sort of mapping  to output a List Of Models.ReservationFlight
            // };
        }

        [HttpPost]
        public ActionResult Post(NewReservationRequest newReservationRequest)
        {
            //TODO: Add some sort of mapping from input Models to Domain object
            List<Passenger> outboundPassengers = null; // TODO: Use => newReservationRequest.GetOutBoundFlight().Passengers;

            Domain.Reservations.Reservation result;

            if (newReservationRequest.IsRoundTrip)
            {
                //TODO: Add some sort of mapping from input Models to Domain object
                List<Passenger> inboundPassengers = null; //TODO: Use => newReservationRequest.GetInBoundFlight().Passengers;

                result = _service.BookOneWayTrip(newReservationRequest.Email, newReservationRequest.CreditCard, newReservationRequest.GetOutBoundFlight().Key, outboundPassengers, newReservationRequest.GetInBoundFlight().Key, inboundPassengers);
            }
            else
            {
                result = _service.BookRoundTrip(newReservationRequest.Email, newReservationRequest.CreditCard, newReservationRequest.GetOutBoundFlight().Key, outboundPassengers);
            }

            if (result == null)
            {
                return BadRequest();
            }

            return Ok(result.ReservationNumber);
        }
    }
}

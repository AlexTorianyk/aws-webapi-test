﻿using System.Collections.Generic;
using Ryanair.Reservation.Domain.Shared.Passenger;

namespace Ryanair.Reservation.Reservations
{
    public class ReservationFlight
    {
        public string Key { get; set; }

        public List<Passenger> Passengers { get; set; }
    }
}

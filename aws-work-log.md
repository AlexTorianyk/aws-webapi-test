# **AWS Practice Test Work log**

## Overview

Feel free to add everything you consider in order to understand why you implement those approaches and what are the benefits them in terms of:

1. Operational
1. Security
1. Reliability
1. Performance
1. Cost optimization

### Part 01 Explain all steps of Deploy serverless architecture

#### 01 - Steps to achive goals

1. Created aws account, set up aws cli, etc. ![image info](./images/part-1/aws-configure.jpg)
2. Created a separate serverless project that did some of the boilerplate code for me.
3. Improted the flights from the data folder after tweaking the format a bit ![image info](./images/part-1/batch-write-flights.jpg)
4. Created the `GetFlights`,`GetReservations` and `AddReservation` functions and tested them locally after tweaking the `template.yaml` file
5. Deployed the serverless application ![image info](./images/part-1/deploying-serverless-app.png)
6. Had to create a new role called RyanairLambda through the IAM Management console since I didn't add DynamoDB permissions in `template.yaml` ![image info](./images/part-1/lambda-role-creation.png)
7. I then set the lambdas to the newly created role. ![image info](./images/part-1/setting-lambda-role.png)
8. And tested the function both on browser ![image info](./images/part-1/testing-in-browser.png)
9. And through Rider ![image info](./images/part-1/testing-in-rider.png)
10. Then I deployed the api through the Gateway API console ![image info](./images/part-1/deploying-api.png)
11. And extracted the OpenAPI definition ![image info](./images/part-1/exporting-openAPI-docs.png)

#### 01 - Beneficts of your solution

I used the `Amazon.Lambda.Serialization.Json.JsonSerializer` which is apparently more efficient(it was stated in the docs). I also have defined some DTO classes so that it's easier to map the DynamoDB responses to C# objecs.

#### 01 - Notes

Ideally, I would create roles in `template.yaml` and do it with the least-priveledge required principle. Probably creating two roles with readonly and writeonly access. I also have trouble understanding how this solution(adding a lambda middleman for db access) is better than the traditional one. Seems like we're just taking extra steps, the articles I've found have mostly used lambdas for reacting to events from the dynamoDB stream, like sending an email when a user registers or something like that.

### Part 03 Explain all steps of Deploy Ryanair API on EC2 instances

#### 03 - Steps to achive goals

_write here_

#### 03 - Beneficts of your solution

_write here_

#### 03 - Link your architecture diagram of your solution

_write here_

#### 03 -Notes

_write here_
